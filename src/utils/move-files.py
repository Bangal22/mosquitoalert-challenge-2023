import shutil
import os

def move_files(origin_path, destination_path, limit) :
    files = os.listdir(origin_path)[:limit]
    for file in files:
        op = os.path.join(origin_path, file)
        od = os.path.join(destination_path, file)
        shutil.move(op, od)

if __name__ == "__main__" :
    train_labels_path = '../../data/labels/train/'
    validation_labels_path = '../../data/labels/validation/'

    train_images_path = '../../data/images/train/'
    validation_images_path = '../../data/images/validation/'
    
    move_files(train_labels_path, validation_labels_path, 700)
    move_files(train_images_path, validation_images_path, 700)