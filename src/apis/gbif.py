import requests
import urllib.request
import os 
import pandas as pd

from io import BytesIO
from PIL import Image
from urllib.parse import urlparse
from requests.models import PreparedRequest
from concurrent.futures import ThreadPoolExecutor

_home = '../../data'
_images_default_path = f'{_home}/external/images/'
_csv_default_path = f'{_home}/external/csv/'
_json_default_path = f'{_home}/external/csv/'

_url_base = "https://www.gbif.org/api/occurrence/search?"
_url_params = {
    "advanced": "false",
    "continent": "EUROPE",
    "locale": "en",
    "mediaType": "stillImage",
    "occurrence_status": "present",
}

class GbifAPI:
    def __init__(self):
        self.species = None

    def get_mosquitos(self, species, limit, offset):
        self.species = species
        url = f'{_url_base}&q={species}&limit={limit}&offset={offset}'
        req = PreparedRequest()
        req.prepare_url(url, _url_params)
        response = requests.get(req.url)

        if response.status_code != 200:
            raise 'Error while processing request'

        mosquitos_information = response.json()['results']
        mosquitos_filtered = list(filter(self.filter_mosquito, mosquitos_information))
        mosquitos_mapped = list(map(self.map_mosquito, mosquitos_filtered))
        return mosquitos_mapped

    def map_mosquito(self, mosquito):
        media = mosquito['media'][0]['identifier']
        width, height = self.get_image_resolution(media)
        return {
            "class_label": mosquito['genericName'].lower(),
            "image_url": media,
            "img_fName": self.get_image_fName(media),
            "img_w": width, 
            "img_h": height,
        }
    
    def filter_mosquito(self, mosquito) :
        if "lifeStage" in mosquito :   
            return mosquito['lifeStage'] == 'Imago'
        return mosquito

    def get_image_resolution(self, image_url) :
        with urllib.request.urlopen(image_url) as response, BytesIO(response.read()) as data:
            img = Image.open(data)
        return img.width, img.height

    def get_image_fName(self, image_url) :
        path = urlparse(image_url).path 
        image = os.path.split(path)
        image, extension = image[1].split('_') 
        return f'{image}.{extension}'
    
    def save_image(self, path, url_img, fName) :
        self.create_path(path)
        blob = requests.get(url_img)
        with open(f'{path}/{fName}', 'wb') as handler:
            handler.write(blob.content)
            
    def save_image_parallelized(self, path, url_img, fName):
        def download_image(url, file_path):
            blob = requests.get(url)
            with open(file_path, 'wb') as handler:
                handler.write(blob.content)

        self.create_path(path)
        file_path = f'{path}/{fName}'

        with ThreadPoolExecutor() as executor:
            executor.submit(download_image, url_img, file_path)
    
    def save_as_csv(self, path, data) :
        self.create_path(path)
        pd.json_normalize(data).to_csv(f'{path}/{self.species}.csv', index= False)
    
    def save_as_json(self, path, data) :
        self.create_path(path)
        pd.json_normalize(data).to_json(f'{path}/{self.species}.json')
        
    def create_path (self, path) :
        if os.path.exists(path) is False:
            os.mkdir(path)
    
def main(args):
    api = GbifAPI()
    mosquitos = api.get_mosquitos(species=args.specie, limit=args.limit, offset=args.offset)
    
    api.save_as_csv(args.csv_path, mosquitos)
    # api.save_as_json(args.json_path, mosquitos)
    for mosquito in mosquitos :
        api.save_image_parallelized(args.imgs_path, mosquito['image_url'], mosquito['img_fName'])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--imgs_path', default = _images_default_path, help = 'where the images files will be stored')
    parser.add_argument('--csv_path', default = _csv_default_path, help = 'where the csv file will be stored')
    parser.add_argument('--json_path', default = _json_default_path, help = 'where the json file will be stored')
    parser.add_argument('--specie', default = 'anopheles', help = 'mosquito specie')
    parser.add_argument('--limit', type = int, default = 5 , help = 'max number of mosquitos')
    parser.add_argument('--offset', type = int, default = 0, help = '')

    args = parser.parse_args()

    main(args)
