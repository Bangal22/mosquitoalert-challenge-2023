import pybboxes as pbx
import pandas as pd
import os

TRAIN_CSV_DEFAULT_PATH = "../../data/csv/train/train.csv"
LABEL_DEFAULT_PATH = "../../data/labels/train"

class YoloConfig:
    def __init__(self) -> None:
        pass

    def voc2yolobbox(self, bbox, width, height):
        return pbx.convert_bbox(
            bbox, from_type="voc", to_type="yolo", image_size=(width, height)
        )

    def csv2label(self, csv_path, label_path):
        df = pd.read_csv(csv_path)
        for i in range(len(df)):
            file_name = df.iloc[i]["img_fName"].split(".")[0]
            f = open(f"{label_path}/{file_name}.txt", "w+")
            width = df.iloc[i]["img_w"]
            height = df.iloc[i]["img_h"]
            bbox = (
                df.iloc[i]["bbx_xtl"],
                df.iloc[i]["bbx_ytl"],
                df.iloc[i]["bbx_xbr"],
                df.iloc[i]["bbx_ybr"],
            )

            x, y, w, h = self.voc2yolobbox(bbox, width, height)
            f.write(f"0 {x} {y} {w} {h}")
            f.close()

def main(args):
    y = YoloConfig()
    y.csv2label(args.csv_path, args.label_path)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("--csv_path", default=TRAIN_CSV_DEFAULT_PATH, help="")
    parser.add_argument("--label_path", default=LABEL_DEFAULT_PATH, help="")

    args = parser.parse_args()

    main(args)
