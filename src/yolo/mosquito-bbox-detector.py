from ultralytics import YOLO

DEFAULT_MODEL = "yolov8s.yaml"
DEFAULT_DATA = "config.yaml"


class MosquitoBBOXDetector:
    def __init__(self, epochs, model, data) -> None:
        self.epochs = epochs
        self.model = model
        self.data = data

    def load_model(self):
        self.model = YOLO(f"{self.model}")

    def train_model(self):
        self.model.train(data=f"{self.data}", epochs=self.epochs)


def main(args):
    detector = MosquitoBBOXDetector(args.epochs, args.model, args.data)
    detector.load_model()
    detector.train_model()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument("--epochs", type=int, default=8, help="number of epochs")
    parser.add_argument("--model", default=DEFAULT_MODEL, help="")
    parser.add_argument("--data", default=DEFAULT_DATA, help="")

    args = parser.parse_args()

    main(args)
